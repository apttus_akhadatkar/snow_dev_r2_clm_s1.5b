﻿/****************************************************************************************
@Name: AddressController.cs
@Author: Mahesh Patel
@CreateDate: 1 Sep 2017
@Description: Resolves ExternalIDs and Upserts addresses using generic API 
@UsedBy: This will be used by Mule as an API call

@ModifiedBy: Varun Shah 
@ModifiedDate: 04 Oct 2017
@ChangeDescription: Added API : Address Sync, Address Suggest & Address Validator
*****************************************************************************************/

using Apttus.SNowPS.Common;
using Apttus.SNowPS.Model;
using Apttus.SNowPS.Respository;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace Apttus.SNowPS.API.Controllers
{
    [ServiceRequestActionFilter]
    public class AddressController : ApiController
    {
        private readonly AddressRepository _address = new AddressRepository();
        private string muleXClientId = ConfigurationManager.AppSettings[Constants.CONFIG_MULEXCLIENTID];
        private string muleXSourceSystem = ConfigurationManager.AppSettings[Constants.CONFIG_MULEXSOURCESYSTEM];
        /// <summary>
        /// Address Advance Search
        /// </summary>
        /// <returns></returns>
        public HttpResponseMessage Get()
        {
            try
            {
                string jsonResponse = string.Empty;
                if (this.Request != null && this.Request.RequestUri != null && !string.IsNullOrEmpty(this.Request.RequestUri.Query))
                {
                    //Get Access Token from Headers
                    var headers = this.Request.Headers;
                    var accessToken = headers.Authorization != null ? Convert.ToString(headers.Authorization) : null;

                    var encodedQuery = this.Request.RequestUri.Query;
                    var decodedQuery = Utilities.GetDecodedQuery(encodedQuery);

                    var reqConfig = new RequestConfigModel();
                    reqConfig.accessToken = accessToken;
                    reqConfig.objectName = Constants.OBJ_ACCOUNTLOCATION;

                    var response = Utilities.GetSearch(decodedQuery, reqConfig);

                    return Utilities.CreateResponse(response);
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, "");
            }
            catch (Exception ex)
            {
                //TODO
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Dictionary<string, object> { { Constants.STR_MESSAGE, ex.Message } });
            }
        }

        /// <summary>
        /// Resolves extrenal ids and upserts account addresses using standard API
        /// </summary>
        /// <param name="dictContent"></param>
        /// <returns></returns>
        [ActionName("Upsert")]
        [HttpPost]
        public HttpResponseMessage UpsertAddress([FromBody] List<Dictionary<string, object>> dictContent)
        {
            try
            {
                if (dictContent != null && dictContent.Count() > 0)
                {
                    var headers = this.Request.Headers;
                    var accessToken = headers.Authorization != null ? Convert.ToString(headers.Authorization) : null;

                    if (!string.IsNullOrEmpty(accessToken))
                    {
                        return _address.UpsertAddress(dictContent, accessToken);
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, new Dictionary<string, object> { { Constants.STR_MESSAGE, Constants.ERR_AUTH_TOKEN_MISSING } });
                    }
                }
                return Request.CreateResponse(HttpStatusCode.BadRequest, "");
            }
            catch (Exception ex)
            {
                //TODO
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Dictionary<string, object> { { Constants.STR_MESSAGE, ex.Message } });
            }
        }

        /// <summary>
        /// Use for Address Sync
        /// </summary>
        /// <param name="dictContent"></param>
        /// <returns></returns>
        [ActionName("sync")]
        [HttpPost]
        public HttpResponseMessage AddressSync([FromBody] Dictionary<string, object> dictContent)
        {
            try
            {
                List<Dictionary<string, object>> lstDictContent = new List<Dictionary<string, object>>();
                lstDictContent.Add(dictContent);
                var headers = this.Request.Headers;
				var accessToken = Utilities.GetAuthToken();
                return _address.SendAddressDataToMule(lstDictContent, accessToken);
                
            }
            catch(Exception ex)
            {
                //TODO
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Dictionary<string, object> { { Constants.STR_MESSAGE, ex.Message } });
            }
        }

        /// <summary>
        /// Use for Address validation
        /// </summary>
        /// <param name="requestContent"></param>
        /// <returns></returns>
        [ActionName("validator")]
        [HttpPost]
        public HttpResponseMessage AddressValidator([FromBody]JObject requestContent)
        {
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                AddressValidationModel responseAddr = JsonConvert.DeserializeObject<AddressValidationModel>(requestContent.ToString());
                return _address.AddressValidator(responseAddr);
            }
            catch (Exception ex)
            {
                //TODO
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Dictionary<string, object> { { Constants.STR_MESSAGE, ex.Message } });
            }
        }

        /// <summary>
        /// Use for Address Search
        /// </summary>
        /// <param name="requestContent"></param>
        /// <returns></returns>
        [ActionName("suggest")]
        [HttpPost]
        public HttpResponseMessage AddressSuggest([FromBody]JObject requestContent)
        {
            try
            {
                JavaScriptSerializer serializer = new JavaScriptSerializer();
                Dictionary<string, string> queryAddr = JsonConvert.DeserializeObject<Dictionary<string, string>>(requestContent.ToString());
                return _address.AddressSuggest(queryAddr);
            }
            catch(Exception ex)
            {
                //TODO
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Dictionary<string, object> { { Constants.STR_MESSAGE, ex.Message } });
            }
        }

    }
}
