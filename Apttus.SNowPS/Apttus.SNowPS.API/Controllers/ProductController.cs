﻿/****************************************************************************************
@Name: ProductController.cs
@Author: Mahesh Patel
@CreateDate: 1 Sep 2017
@Description: Gets the product details, maps to mule fields and call mule api for sync 
@UsedBy: This will be used by Apttus as an API call to Mule

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/

using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using Apttus.SNowPS.Respository;
using Apttus.SNowPS.Common;
using Apttus.SNowPS.Model;

namespace Apttus.SNowPS.API.Controllers
{
    [ServiceRequestActionFilter]
    public class ProductController : ApiController
    {
        private readonly ProductRepository _products = new ProductRepository();

        [ActionName("Sync")]
        [HttpPost]
        public HttpResponseMessage SyncToMule([FromBody]Dictionary<string, object> dictContent)
        {
            try
            {
                if (dictContent != null && dictContent.Count() > 0)
                {
					//Get case-insensitive 
					var dicProduct = Utilities.GetCaseIgnoreSingleDictContent(dictContent);

                    var productId = dicProduct.ContainsKey(Constants.FIELD_ID) && dicProduct[Constants.FIELD_ID] != null ? Convert.ToString(dicProduct[Constants.FIELD_ID]) : null;

					//Generate authentication token
					var accessToken = Utilities.GetAuthToken();

                    //Get Product Json For Mule
                    var productJsonForMule = _products.GetProducts(productId, accessToken);

                    //Call Mule API
                    if (!string.IsNullOrEmpty(productJsonForMule))
                    {
                        MuleHeaderModel objMuleHeaderModel = new MuleHeaderModel();
                        objMuleHeaderModel.APIName = ConfigurationManager.AppSettings["ProductSyncAPI"];
                        objMuleHeaderModel.MuleUrl = ConfigurationManager.AppSettings["MockAPIUrl"];
                        objMuleHeaderModel.XClientId = ConfigurationManager.AppSettings["Mule_X-Client-ID"];
                        objMuleHeaderModel.HttpContentStr = new StringContent(productJsonForMule, Encoding.UTF8, "application/json");
                        objMuleHeaderModel.AccessToken = Utilities.GetMuleAuthToken();

                        var muleResponseMessage = HttpActions.PostRequestMule(objMuleHeaderModel);

                        //Get Product ID from Mule Response
                        if(muleResponseMessage != null && muleResponseMessage.IsSuccessStatusCode)
                        {
                            var muleResponseString = muleResponseMessage.Content.ReadAsStringAsync().Result;
                            var muleResponseObj = JsonConvert.DeserializeObject<Dictionary<string,object>>(muleResponseString);

                            if(muleResponseObj != null && muleResponseObj.Count > 0)
                            {
                                var extProductId = Convert.ToString(muleResponseObj["consumerID"]);

                                //TODO: Update ExternalID of Product in Apttus
                                var updateResponse = Utilities.UpdateMuleResponse(productId, extProductId, Constants.OBJ_PRODUCT);

                                return Utilities.CreateResponse(updateResponse);
                            }
                        }
                        else
                        {
                            return muleResponseMessage;
                        }
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, new Dictionary<string, object> { { Constants.STR_MESSAGE, "Error Fetching Product Details." } });
                    }
                }

                return Request.CreateResponse(HttpStatusCode.BadRequest, "");
            }
            catch (Exception ex)
            {
                //TODO
                return Request.CreateResponse(HttpStatusCode.BadRequest, new Dictionary<string, object> { { Constants.STR_MESSAGE, ex.Message+ex.StackTrace } });
            }
        }
    }
}
