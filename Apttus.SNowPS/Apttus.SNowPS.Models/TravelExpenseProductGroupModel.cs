﻿/*************************************************************
@Name: TravelExpenseProdcutModel.cs
@Author: Nilesh Keshwala    
@CreateDate: 08-Nov-2017
@Description: This class contains classes & properties for 'Prodcut group' and related objects.
@UsedBy: Description of how this class is being used
******************************************************************/

namespace Apttus.SNowPS.Model
{
    public class TravelExpenseProductGroupModel
    {
        public LookUp ProductId { get; set; }
        public LookUp ProductGroupId { get; set; }
    }

}
