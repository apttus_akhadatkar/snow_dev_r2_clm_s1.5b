﻿/****************************************************************************************
@Name: AccountModel.cs
@Author: Akash Kakadiya
@CreateDate: 1 Sep 2017
@Description: Defines Common Methods 
@UsedBy: This will be used by account releted functions 

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Apttus.SNowPS.Model
{
    public class AccountModel
    {
        public string sourceSystem { get; set; }
        public string accountId { get; set; }

        [JsonProperty(PropertyName = "Id")]
        public string srcPartyId { get; set; }

        [JsonProperty(PropertyName = "ExternalId")]
        public string mdmId { get; set; }

        public string accountName { get; set; }

        [JsonProperty(PropertyName = "Name")]
        public string partyNm { get; set; }

        //[JsonIgnore]
        [JsonProperty(PropertyName = "Type")]
        public string partyTypCd { get; set; }

        public string eventType { get; set; }

        public string boClassCode { get; set; }

        [JsonProperty(PropertyName = "ModifiedOn")]
        public string lastUpdatedDateTime { get; set; }

        [JsonProperty(PropertyName = "ext_ContractRvw")]
        public string ContractReview { get; set; }

        public List<AddressModel> addresses { get; set; }
    }
}
