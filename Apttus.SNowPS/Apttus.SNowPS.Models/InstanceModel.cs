﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Apttus.SNowPS.Model
{
    public class InstanceModel
    {
        public string inst { get; set; }
        public string instName { get; set; }
        public string instLIId { get; set; }
        public bool IsProdInstance { get; set; }
        public string instLINumInstMap { get; set; }
        public string Name { get; set; }
        public List<InstanceProduct> Products { get; set; }

        public List<String> SoldOnProducts { get; set; }
        public List<String> SoldOnProductsNumbers { get; set; }
        public List<String> InstalledOnProducts { get; set; }

        public List<String> InstalledOnProductsNumbers { get; set; }

        public bool HasError { get; set; }
        public string Error { get; set; }

    }

    public class InstanceProduct
    {
        public string prodLIId { get; set; }
        public string instId { get; set; }
        public string prodLIName { get; set; }
        public string prodLINumInstMap { get; set; }
        public string Id { get; set; }
        public string Name { get; set; }
        public string instName { get; set; }
        public bool SoldOn { get; set; }
        public bool IsSoldOnDisabled { get; set; }
        public bool InstalledOn { get; set; }
        public bool IsInstalledOnDisabled { get; set; }
        public bool IsProductionInstance { get; set; }
    }
}