﻿/****************************************************************************************
@Name: ParticipantRepository.cs
@Author: Mahesh Patel
@CreateDate: 13 Nov 2017
@Description: Participant Related Logic
@UsedBy: This will be used by ParticipantController.cs

@ModifiedBy: 
@ModifiedDate: 
@ChangeDescription: 
*****************************************************************************************/

using Apttus.SNowPS.Common;
using Apttus.SNowPS.Model;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Apttus.SNowPS.Repository.Integration
{
    public class ParticipantsRepository
    {
        #region Fields
        /// <summary>
        /// Static Object of Participants Repository
        /// </summary>
        private static ParticipantsRepository participantRepository = new ParticipantsRepository();
        public string AccessToken { get; set; }
        #endregion

        /// <summary>
        /// Use to Get Single Instance of AgreementClauseRepository
        /// </summary>
        /// <param name="authToken"></param>
        /// <returns></returns>
        public static ParticipantsRepository Instance(string accessToken)
        {
            if (participantRepository != null)
            {
                participantRepository.AccessToken = accessToken;
            }
            return participantRepository;
        }

        /// <summary>
        /// Upsert Stackholders in Participant Object
        /// </summary>
        /// <param name="dictContent"></param>
        /// <param name="accessToken"></param>
        /// <returns></returns>
        public HttpResponseMessage UpsertParticipant(List<Dictionary<string, object>> dictContent)
        {
            try
            {
                var errors = new List<ErrorInfo>();
                var responseParticipant = new HttpResponseMessage();

                //Convert content to case-insensitive
                dictContent = Utilities.GetCaseIgnoreDictContent(dictContent);

                //Resolve Surf User/Emp Id with Apttus User Id
                var reqConfig = new RequestConfigModel
                {
                    accessToken = AccessToken,
                    select = new string[] { Constants.FIELD_NAME, Constants.FIELD_EXTERNALID },

                    objectName = Constants.OBJ_USER,
                    externalFilterField = Constants.FIELD_USER_ID,
                    apttusFilterField = Constants.FIELD_EXTERNALID,
                    resolvedID = Constants.FIELD_USER_ID
                };

                Utilities.ResolveExternalID(dictContent, reqConfig, errors);

                //Get RoleID from Role
                reqConfig.objectName = Constants.OBJ_ROLE;
                reqConfig.externalFilterField = Constants.NODE_ROLE;
                reqConfig.apttusFilterField = Constants.FIELD_EXTERNALID;
                reqConfig.resolvedID = Constants.FIELD_ROLEID;

                Utilities.ResolveExternalID(dictContent, reqConfig, errors);

                //Remove unresolved requests
                Utilities.RemoveUnresolvedRequests(dictContent, errors, Constants.FIELD_EXTERNALID);

                //Remove RoleID
                foreach (var participant in dictContent)
                {
                    if (participant.ContainsKey(Constants.FIELD_ROLEID))
                        participant.Remove(Constants.FIELD_ROLEID);
                }

                //Get Participant Roles
                var participantRoles = new List<Dictionary<string, object>>();
                GetParticipantRoles(dictContent, participantRoles);

                //Context Object and Participant Object
                Parallel.ForEach(dictContent, participant =>
                {
                    if (participant.ContainsKey(Constants.FIELD_QUOTEID))
                    {
                        var contextObject = new { Id = participant[Constants.FIELD_QUOTEID], Type = Constants.OBJ_QUOTE };
                        participant[Constants.FIELD_CONTEXTOBJECT] = contextObject;
                        participant.Remove(Constants.FIELD_QUOTEID);
                    }

                    if (participant.ContainsKey(Constants.FIELD_USER_ID))
                    {
                        var participantObject = new { Id = participant[Constants.FIELD_USER_ID], Type = Constants.OBJ_USER };
                        participant[Constants.FIELD_PARTICIPANT] = participantObject;
                        participant.Remove(Constants.FIELD_USER_ID);
                    }
                });

                if (dictContent != null && dictContent.Count > 0)
                {
                    //Upsert Participant
                    reqConfig.objectName = Constants.OBJ_PARTICIPANT;
                    reqConfig.UpsertKey = Constants.FIELD_EXTERNALID;
                    responseParticipant = Utilities.Upsert(dictContent, reqConfig);
                    responseParticipant = Utilities.CreateResponse(responseParticipant, errors);

                    if (responseParticipant.IsSuccessStatusCode && participantRoles != null && participantRoles.Count > 0)
                    {
                        //Create Participant Roles
                        var respParticipantRoles = UpsertParticipantRoles(participantRoles);

                        //Merge responses
                        responseParticipant = Utilities.MergeErrorResponse(responseParticipant, respParticipantRoles);
                    }
                }
                else
                {
                    var bulkResponseModel = new BulkOperationResponseModel();
                    var jsonResponse = JsonConvert.SerializeObject(bulkResponseModel);
                    responseParticipant.Content = Utilities.CreateJsonContent(jsonResponse);
                    Utilities.CreateResponse(responseParticipant, errors);
                }

                return responseParticipant;
            }
            catch
            {
                //TODO
                throw;
            }
        }

        /// <summary>
        /// Get Participant Roles from Request
        /// </summary>
        /// <param name="participants"></param>
        /// <param name="participantRoles"></param>
        private void GetParticipantRoles(List<Dictionary<string, object>> participants, List<Dictionary<string, object>> participantRoles)
        {
            foreach (var participant in participants)
            {
                if (participant.ContainsKey(Constants.NODE_ROLE) && participant.ContainsKey(Constants.FIELD_EXTERNALID))
                {
                    var participantRole = new Dictionary<string, object>();

                    if (participant.ContainsKey(Constants.FIELD_EXTERNALID))
                        participantRole.Add(Constants.FIELD_EXTERNALID, participant[Constants.FIELD_EXTERNALID]);

                    if (participant.ContainsKey(Constants.FIELD_NAME) && participant.ContainsKey(Constants.NODE_ROLE))
                        participantRole.Add(Constants.FIELD_NAME, participant[Constants.FIELD_NAME] + "-" + participant[Constants.NODE_ROLE]);

                    if (participant.ContainsKey(Constants.NODE_ROLE))
                    {
                        participantRole.Add(Constants.NODE_ROLE, participant[Constants.NODE_ROLE]);
                        participant.Remove(Constants.NODE_ROLE);
                    }

                    participantRoles.Add(participantRole);
                }
            }
        }

        private HttpResponseMessage UpsertParticipantRoles(List<Dictionary<string, object>> participantRoles)
        {
            var errors = new List<ErrorInfo>();
            var responseParticipantRoles = new HttpResponseMessage();

            //Convert content to case-insensitive
            participantRoles = Utilities.GetCaseIgnoreDictContent(participantRoles);

            //Resolve Surf User/Emp Id with Apttus User Id
            var reqConfig = new RequestConfigModel();
            reqConfig.accessToken = AccessToken;
            reqConfig.select = new string[] { Constants.FIELD_NAME, Constants.FIELD_EXTERNALID };

            //Get ParticipantID from Participant
            reqConfig.objectName = Constants.OBJ_PARTICIPANT;
            reqConfig.externalFilterField = Constants.FIELD_EXTERNALID;
            reqConfig.apttusFilterField = Constants.FIELD_EXTERNALID;
            reqConfig.resolvedID = Constants.FIELD_PARTICIPANTID;

            Utilities.ResolveExternalID(participantRoles, reqConfig, errors);

            //Remove unresolved requests
            Utilities.RemoveUnresolvedRequests(participantRoles, errors, Constants.FIELD_EXTERNALID);

            if (participantRoles != null && participantRoles.Count > 0)
            {
                //Upsert Participant
                reqConfig.objectName = Constants.OBJ_PARTICIPANTROLE;
                reqConfig.UpsertKey = Constants.FIELD_EXTERNALID;
                responseParticipantRoles = Utilities.Upsert(participantRoles, reqConfig);
            }
            else
            {
                var bulkResponseModel = new BulkOperationResponseModel();
                var jsonResponse = JsonConvert.SerializeObject(bulkResponseModel);
                responseParticipantRoles.Content = Utilities.CreateJsonContent(jsonResponse);
            }

            return Utilities.CreateResponse(responseParticipantRoles, errors);
        }
    }
}
